#ifndef SKINAPPLIER_H
#define SKINAPPLIER_H

#include <QWidget>

namespace Ui {
class SkinApplier;
}

class SkinApplier : public QWidget
{
    Q_OBJECT

public:
    explicit SkinApplier(QWidget *parent = nullptr);
    ~SkinApplier();

    void SetSkin(std::string);

private slots:
    void on_open_skin_file_clicked();
    void on_apply_skin_clicked();
    void on_open_skins_folder_clicked();
    void on_load_at_startup_stateChanged(int);

private:
    Ui::SkinApplier *ui;
    QApplication *app;
};

#endif // SKINAPPLIER_H
