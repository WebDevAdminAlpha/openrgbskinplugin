#include "SkinSettings.h"
#include "OpenRGBSkinPlugin.h"
#include <fstream>
#include "filesystem.h"

const std::string SkinSettings::settings_folder = "plugins/settings/";
const std::string SkinSettings::settings_filename = "skins.json";

void SkinSettings::SaveSettings(json settings)
{
    if(!CreateSettingsDirectory())
    {
        printf("Cannot create settings directory.\n");
        return;
    }

    std::ofstream SFile((OpenRGBSkinPlugin::RMPointer->GetConfigurationDirectory() + settings_folder + settings_filename), std::ios::out | std::ios::binary);

    if(SFile)
    {
        try{
            SFile << settings.dump(4);
            SFile.close();
            printf("Skins settings file successfully written.\n");
        }
        catch(const std::exception& e)
        {
            printf("Cannot write skins settings file.\n %s\n", e.what());
        }
        SFile.close();
    }
}

json SkinSettings::LoadSettings()
{
    json Settings;

    std::ifstream SFile(OpenRGBSkinPlugin::RMPointer->GetConfigurationDirectory() + settings_folder + settings_filename, std::ios::in | std::ios::binary);

    if(SFile)
    {
        try
        {
            SFile >> Settings;
            SFile.close();
        }
        catch(const std::exception& e)
        {
             printf("Cannot read skins settings file.\n %s\n", e.what());
        }
    }

    return Settings;
}

bool SkinSettings::CreateSettingsDirectory()
{
    std::string settings_directory = OpenRGBSkinPlugin::RMPointer->GetConfigurationDirectory() + settings_folder;

    if(!filesystem::exists(settings_directory))
    {
        return filesystem::create_directory(settings_directory);
    }

    return true;
}

