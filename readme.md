# Skin plugin 

[![pipeline status](https://gitlab.com/OpenRGBDevelopers/openrgbskinplugin/badges/master/pipeline.svg)](https://gitlab.com/OpenRGBDevelopers/openrgbskinplugin/-/commits/master)

## What is this?

This is a plugin for [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB) that allows you to customize the look and feel of OpenRGB

## How do I install it?

### **windows**

1. download the latest [Windows pipeline](https://gitlab.com/OpenRGBDevelopers/openrgbskinplugin/-/jobs/artifacts/master/download?job=Windows%2064)

2. copy ``OpenRGBSkinPlugin.dll`` from the 7z file to  ``C:\Users\Username\appdata\roaming\OpenRGB\plugins``

3. Run the latest [Pipeline build of OpenRGB(At least until 0.6 is out)](https://gitlab.com/Calcprogrammer1/OpenRGB/-/jobs/artifacts/master/download?job=Windows%2064)

### **linux**

1. download the latest [linux pipeline](https://gitlab.com/OpenRGBDevelopers/openrgbskinplugin/-/jobs/artifacts/master/download?job=Linux%2064)

2. Copy ``libOpenRGBSkinPlugin.so`` from the 7z to ``~/.config/OpenRGB/plugins``

3. Run the latest [Pipeline build of OpenRGB(At least until 0.6 is out)](https://gitlab.com/Calcprogrammer1/OpenRGB/-/jobs/artifacts/master/download?job=Linux%2064%20AppImage)


