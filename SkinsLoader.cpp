#include "SkinsLoader.h"
#include "OpenRGBSkinPlugin.h"
#include "filesystem.h"

#include <QFile>
#include <QTextStream>

SkinsLoader::SkinsLoader()
{

}

std::vector<std::string> SkinsLoader::GetSkinFileNames()
{
    std::string path = OpenRGBSkinPlugin::RMPointer->GetConfigurationDirectory() + "plugins/skins";
    std::vector<std::string> filenames;

    for (filesystem::directory_entry entry : filesystem::directory_iterator(path))
    {
        filesystem::path filename = entry.path().filename();

        if(filename.extension() == ".qss")
        {
            filenames.push_back(filename.u8string());
        }
    }

    return filenames;
}

QString SkinsLoader::LoadFile(QString fileName)
{
    QFile f(fileName);

    if (!f.open(QFile::ReadOnly | QFile::Text))
    {
        return "";
    }

    QTextStream in(&f);

    return in.readAll();
}

QString SkinsLoader::LoadFileFromSkinsFolder(QString fileName)
{
    return LoadFile(QString::fromStdString(OpenRGBSkinPlugin::RMPointer->GetConfigurationDirectory() + "plugins/skins/" + fileName.toStdString()));
}
