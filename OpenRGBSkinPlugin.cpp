#include "OpenRGBSkinPlugin.h"
#include <QHBoxLayout>
#include "SkinApplier.h"
#include "SkinSettings.h"
#include "filesystem.h"

bool OpenRGBSkinPlugin::DarkTheme = false;
ResourceManager* OpenRGBSkinPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBSkinPlugin::Initialize(bool Dt, ResourceManager *RM)
{
    PInfo.PluginName         = "Skins";
    PInfo.PluginDescription  = "Apply skins";
    PInfo.PluginLocation     = "SettingsTabBar";
    PInfo.HasCustom          = true;
    PInfo.PluginLabel        = new QLabel("Skins");

    RMPointer                = RM;
    DarkTheme                = Dt;

    return PInfo;
}

QWidget* OpenRGBSkinPlugin::CreateGUI(QWidget* parent)
{    
    // Make sure our skins folder is created
    std::string skins_dir = RMPointer->GetConfigurationDirectory() + "plugins/skins";
    filesystem::create_directories(skins_dir);

    SkinApplier* skin_applyer = new SkinApplier(parent);

    // Check settings for skin auto load
    json settings = SkinSettings::LoadSettings();
    if(settings.contains("load_at_startup"))
    {
        std::string skin_file_name = settings["load_at_startup"];
        skin_applyer->SetSkin(skin_file_name);
    }


    return skin_applyer;
}
